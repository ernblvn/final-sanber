<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create(){
        return view('ask');
    }

    public function store(Request $request){
        // fungsi validasi
        $request->validate([
            'kategori' => 'required',
            'pertanyaan' => 'required',
        ],
        [
            'kategori.required' => 'kategori tidak boleh kosong',
            'pertanyaan.required'  => 'pertanyaan tidak boleh kosong',
        ]
    );
    
        $pertanyaan = new Pertanyaan;
        $pertanyaan->title = $request["kategori"];
        $pertanyaan->body = $request["pertanyaan"];
        $pertanyaan->save();

        return redirect('/ask');
    }

    // public function index(){
    //     $pertanyaan = DB::table('pertanyaan')->get();
 
    //     return view('ask', compact('pertanyaan'));
    // }

    // public function show($id){
    //     $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
    //     // dd($cast);

    //     return view('cast.show', compact('cast'));
    // }

    // public function edit($id){
    //     $pertanyaan = DB::table('cast')->where('id', $id)->first();
    //     // dd($cast);

    //     return view('cast.edit', compact('cast'));
    // }

    // public function update(Request $request, $id){
    //     // fungsi validasi
    //     $request->validate([
    //         'nama' => 'required',
    //         'umur' => 'required',
    //         'bio' => 'required',
    //     ],
    //     [
    //         'nama.required' => 'Nama tidak boleh kosong',
    //         'umur.required'  => 'Umur tidak boleh kosong',
    //         'bio.required'  => 'Bio tidak boleh kosong',
    //     ]
    // );

    // DB::table('cast')
    //           ->where('id', $id)
    //           ->update(
    //                 [
    //                   'nama' => $request['nama'],
    //                   'umur' => $request['umur'],
    //                   'bio' => $request['bio']
    //                 ]
    //             );
    //             return redirect('/cast');
    // }

    // public function destroy($id){

    //     DB::table('cast')->where('id', '=', $id)->delete();

    //     return redirect('/cast');
    // }
}
