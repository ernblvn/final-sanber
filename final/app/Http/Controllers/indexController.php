<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;

class indexController extends Controller
{
    public function rumah(){
        return view('auth.register');
    }

    public function kategori(){
        return view('halaman.kategori');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'gambar' => 'required',
        ],
        [
            'nama.required' => 'Nama kategori tidak boleh kosong',
            'gambar.required'  => 'File harus diisi',
        ]
    );

        DB::table('kategori')->where('id', $id)->first()->insert(
            [
             'nama' => $request['nama'],
             'gambar' => $request['gambar']
            ]
        );
        return redirect('/kategori');

        // dd($request->all());
    }

    public function dash(){
        return view('halaman.dashboard');
    }

    public function profil(){
        return view('halaman.profil');
    }

    public function ask(){
        return view('halaman.tanya');
    }

    public function ans(){
        return view('halaman.jawab');
    }

    public function balas(Request $request){
        $request->validate([
            'kategori' => 'required',
            'jawab' => 'required',
        ],
        [
            'kategori.required' => 'Nama kategori tidak boleh kosong',
            'jawab.required'  => 'Jawaban tidak boleh kosong',
        ]
    );

        DB::table('balasan')->insert(
            [
             'katehori' => $request['kategori'],
             'jawab' => $request['jawab']
            ]
        );
        return redirect('/jawab');

        // dd($request->all());
    }
}
