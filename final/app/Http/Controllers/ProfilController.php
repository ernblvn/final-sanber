<?php

namespace App\Http\Controllers;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;




class ProfilController extends Controller
{

    

    public function edit(Request $request)
    {
        return view('profile.edit', [
            'user' => $request->user()
        ]);
    }

    public function update(Request $request)
    {
        $request->user()->update(
            $request->all()
        );
        alert()->success('Berhasil','Telah Update');


        return redirect()->route('halaman.profil');
    }

    }

