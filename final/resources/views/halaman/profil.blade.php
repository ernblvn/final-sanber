@extends('partials.master')

@section('title')
    Profile
@endsection

@section('content')
    Nama : {{ Auth::user()->name }} <br><br><br>
    E-mail : {{ Auth::user()->email }} <br><br><br>
    <a href="/profile" class="btn btn-primary">Edit Profile</a>
@endsection