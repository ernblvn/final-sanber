@extends('partials.master')

@section('title')
Kategori
@endsection

@section('content')
{{-- isi table crud buat kategori--}}
<div class="container">
  <form action="/kategori" method="post" enctype="multipart/form-data">
    @csrf
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-7">
                <div class="p-5">
                  <div class="form-group">
                    <label>Nama Kategori</label>
                      <input type="text" name="nama" class="form-control">
                        </div>
                          @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                            <div class="form-group">
                              <label>Gambar Kategori</label>
                              <input type="file" name="gambar">
                            </div>
                            @error('gambar')
                              <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                              <button type="submit" class="btn btn-primary">Tambah</button>
                              <a href="/kategori"><button type="button" class="btn btn-primary">Batal</button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </form>
</div>
    
@endsection