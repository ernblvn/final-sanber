@extends('partials.master')

@section('title')
Pertanyaan
@endsection

@section('content')
  <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                   
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Masukkan pertanyaan</h1>
                            </div>
                            <form class="user">
                               
                                <div class="form-group">
                                    <input type="kategori" class="form-control form-control-user"
                                        placeholder="kategori">
                                </div>
                                <div class="form-group">
                                    {{-- <input type="pertanyaan" class="form-control form-control-user"
                                        placeholder="pertanyaan"> --}}
                                        <textarea name="" id="" class="form-control form-control-user" placeholder="pertanyaan"></textarea>
                                </div>
                                
                                <button type="submit" class="btn btn-primary"> {{ __('Tambah') }}</button>
                                <hr>
                              
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    
@push('script')
<script src="https://cdn.tiny.cloud/1/mexdpqdchnr6vjhrz2ftkb0v0l17e9ncswfb5gd8k2tzagl0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@endsection
