@extends('partials.master')

@section('title')
    Dashboard
@endsection

@section('content')
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Pertanyaan</th>
        <th scope="col">Jawaban</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
        <td>Do you have brain?</td>
        <td>NO :)</td>
        <td>
            <form action="/" method="POST">
                @csrf
                @method('delete')
                  <a href="/" class="btn btn-info btn-sm">Detail</a>
                  <a href="/" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
        </td>
      </tr>
      <tr>
        <th scope="row">2</th>
        <td>Is IDE similar with Text Editor?</td>
        <td>Idk, I'm Sorry</td>
        <td>
            <form action="#" method="POST">
                @csrf
                @method('delete')
                  <a href="#" class="btn btn-info btn-sm">Detail</a>
                  <a href="#" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
        </td>
      </tr>
      <tr>
        <th scope="row">3</th>
        <td>R U Tired?</td>
        <td>YEAHHH</td>
        <td>
            <form action="#" method="POST">
                @csrf
                @method('delete')
                  <a href="#" class="btn btn-info btn-sm">Detail</a>
                  <a href="#" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
        </td>
      </tr>
    </tbody>
  </table>
@endsection