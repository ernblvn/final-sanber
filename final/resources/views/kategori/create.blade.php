@extends('partials.master')

@section('title')
    Tambah Kategori
@endsection

@section('content')

<form action="/kategori" method="POST"> 
    @csrf

    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>Gambar</label>
      <input type="text" name="gambar" class="form-control">
    </div>
    @error('gambar')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror


    <button type="submit" class="btn btn-primary">Create</button>
  </form>

@endsection
