<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PertanyaanController;

Route::get('/master', function () {
    return view('partials.master');
});

Route::get('/', 'indexController@rumah');


Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', 'ProfilController@edit')->name('profile.edit');
});
Route::group(['middleware' => 'auth'], function () {

    Route::get('profile', 'ProfilController@edit')
        ->name('profile.edit');

    Route::patch('profile', 'ProfilController@update')
        ->name('halaman.profil');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/regis', function(){
    return view('regis');
});

Route::get('/kategori', 'indexController@kategori');
Route::post('/kategori', 'indexController@store');
Route::get('/dashboard', 'indexController@dash');
Route::get('/profil', 'indexController@profil');
Route::get('/ask', 'indexController@ask');
Route::get('/answer', 'indexController@ans');
Route::post('/answer', 'indexController@balas');


// CRUD Kategori
// Route::get('/kategori/create', 'KategoriController@create');
// Route::post('/kategori', 'KategoriController@store');
// Route::get('/kategori', 'KategoriController@index');
// Route::get('/kategori/{kategori_id}', 'KategoriController@show');
// Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
// Route::put('/kategori/{kategori_id}', 'KategoriController@update');
// Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

// CRUD pertanyaan

Route::get('/post', function () {
    return redirect('/posts');
});
Route::resource('posts', PostController::class);


